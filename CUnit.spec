%global _version 2.1-3

Name: CUnit
Version: 2.1.3
Release: 21
Summary: A Unit Testing Framework for C
License: LGPLv2+
URL: http://cunit.sourceforge.net/
Source0: http://download.sourceforge.net/cunit/%{name}-%{_version}.tar.bz2

BuildRequires: automake git libtool

%description
CUnit is a lightweight system for writing, administering, and running unit tests in C
It provides C programmers a basic testing functionality with a flexible variety of user
interfaces.

CUnit is built as a static library which is linked with the user's testing code.  It
uses a simple framework for building test structures, and provides a rich set of
assertions for testing common data types. In addition, several different interfaces are
provided for running tests and reporting results.

%package devel
Summary: Header files and libraries for CUnit development
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
This package contains the header files and libraries for CUnit development.

%package_help

%prep
%autosetup -n %{name}-%{_version} -p1 -Sgit
find -name *.c -exec chmod -x {} \;

%build
autoreconf -fi
%configure --disable-static
%make_build

%install
%make_install
find %{buildroot} -type f -name "*.la" -delete -print
mkdir -p %{buildroot}%{_docdir}/%{name}
mv %{buildroot}%{_prefix}/doc/%{name} %{buildroot}%{_docdir}/%{name}/html

for f in AUTHORS ChangeLog COPYING NEWS README TODO VERSION ; do
    install -p -m0644 -D $f %{buildroot}%{_docdir}/%{name}/${f}
done

%pre

%preun

%ldconfig_scriptlets

%files
%license %{_docdir}/%{name}/AUTHORS
%license %{_docdir}/%{name}/COPYING
%{_datadir}/%{name}/
%{_libdir}/libcunit.so.*
%dir %{_docdir}/%{name}
%{_docdir}/%{name}/ChangeLog
%{_docdir}/%{name}/NEWS
%{_docdir}/%{name}/README
%{_docdir}/%{name}/TODO
%{_docdir}/%{name}/VERSION

%files devel
%{_docdir}/%{name}/html/
%{_includedir}/%{name}/
%{_libdir}/libcunit.so
%{_libdir}/pkgconfig/cunit.pc

%files help
%{_mandir}/man3/CUnit.3*

%changelog
* Mon Oct 21 2019 chengquan<chengquan3@huawei.com> - 2.1.3-21
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix package bug

* Fri Sep 27 2019 chengquan<chengquan3@huawei.com> - 2.1.3-20
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add help package

* Tue Aug 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.1.3-19
- Package init
